<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Companies extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	// Frontend User CRUD
	public function index()
	{
		$current_id = $this->uri->segment(5);
		$crud = $this->generate_crud('companies');
		$crud->columns('title', 'parent_id', 'created', 'updated');
		$crud->display_as('parent_id', 'Parent');

		$crud->set_relation('parent_id', 'companies', 'title', "parent_id IS NULL AND id != '$current_id'");
		$crud->set_field_upload('image', COMPANY_IMAGES_PATH);

		$crud->field_type('website', 'string');

		$this->mPageTitle = 'Companies';
		$this->render_crud();
	}

}
