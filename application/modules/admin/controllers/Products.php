<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	// Frontend User CRUD
	public function index()
	{
		$current_id = $this->uri->segment(5);
		$crud = $this->generate_crud('products');
		$crud->columns('title', 'barcode', 'category_id', 'company_id', 'created', 'updated');
		$crud->display_as('category_id', 'Category');
		$crud->display_as('company_id', 'Brand');

		//$crud->set_relation('parent_id', 'companies', 'title', "parent_id IS NULL AND id != '$current_id'");
		$crud->set_relation('category_id', 'categories', 'title', "parent_id IS NOT NULL");
		$crud->set_relation('company_id', 'companies', 'title', "parent_id IS NOT NULL");
		$crud->set_relation_n_n('alternate_products', 'alternate_products_junction', 'products', 'product_id', 'alternate_product_id', 'title', null, "id != '$current_id'");
		$crud->set_field_upload('image', PRODUCT_IMAGES_PATH);

		$crud->field_type('website', 'string');

		$this->mPageTitle = 'Products';
		$this->render_crud();
	}

	// Frontend User CRUD
	public function imported_products()
	{
		$current_id = $this->uri->segment(5);
		$crud = $this->generate_crud('imported_products');
		$crud->set_theme('datatables');
		$crud->columns('title', 'barcode', 'category_id', 'company_id', 'created', 'updated');
		$crud->display_as('category_id', 'Category');
		$crud->display_as('company_id', 'Brand');

		//$crud->set_relation('parent_id', 'companies', 'title', "parent_id IS NULL AND id != '$current_id'");
		$crud->set_relation('category_id', 'categories', 'title', "parent_id IS NOT NULL");
		$crud->set_relation('company_id', 'companies', 'title', "parent_id IS NOT NULL");
		$crud->set_relation_n_n('alternate_products', 'imported_products_junction', 'products', 'product_id', 'alternate_product_id', 'title', null, "id != '$current_id'");
		$crud->set_field_upload('image', PRODUCT_IMAGES_PATH);

		$crud->field_type('website', 'string');

		$this->mPageTitle = 'Imported Products';
		$crud->unset_add();
		$crud->add_action('Publish', '', 'admin/products/publish_alternate_product');

		$this->render_crud();
	}

	// Frontend User CRUD
	public function user_added_products()
	{
		$current_id = $this->uri->segment(5);
		$crud = $this->generate_crud('user_added_products');
		$crud->set_theme('datatables');
		$crud->columns('title', 'barcode', 'category_id', 'company_id', 'created', 'updated');
		$crud->display_as('category_id', 'Category');
		$crud->display_as('company_id', 'Brand');

		//$crud->set_relation('parent_id', 'companies', 'title', "parent_id IS NULL AND id != '$current_id'");
		$crud->set_relation('category_id', 'categories', 'title', "parent_id IS NOT NULL");
		$crud->set_relation('company_id', 'companies', 'title', "parent_id IS NOT NULL");
		$crud->set_relation_n_n('alternate_products', 'user_added_products_junction', 'products', 'product_id', 'alternate_product_id', 'title', null, "id != '$current_id'");
		$crud->set_field_upload('image', PRODUCT_IMAGES_PATH);

		$crud->field_type('website', 'string');

		$this->mPageTitle = 'User Added Products';
		$crud->unset_add();
		$crud->add_action('Publish', '', 'admin/products/publish_user_added_product');
		$this->render_crud();
	}

	public function publish_alternate_product($product_id) {
		$this->load->model('Imported_products_model', 'imported_products');
		$alternate_product = $this->imported_products->get($product_id);
		$newId = $this->imported_products->publish($alternate_product);
		redirect("admin/products/index/edit/{$newId}");
		exit;
	}

	public function publish_user_added_product($product_id) {
		$this->load->model('User_added_products_model', 'user_added_products');
		$alternate_product = $this->user_added_products->get($product_id);
		$newId = $this->user_added_products->publish($alternate_product);
		redirect("admin/products/index/edit/{$newId}");
		exit;
	}
}
