<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	// Frontend User CRUD
	public function index()
	{
		$current_id = $this->uri->segment(5);
		$crud = $this->generate_crud('news');
		$crud->columns('title', 'image', 'created', 'updated');

		$crud->set_field_upload('image', PRODUCT_IMAGES_PATH);

		$crud->field_type('website', 'string');

		$this->mPageTitle = 'News';
		$this->render_crud();
	}

}
