<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home page
 */
class Rest extends MY_Controller {

	private $keys = array(
		'1f6eaf7b7c7baecab9a68dfbc4446146'
	);

	public function authenticate() {
		if( $this->input->server('REQUEST_METHOD') != 'POST' ) {
			$this->show_error();
			return;
		}
		$key = $this->input->post('key', true);
		if( in_array($key, $this->keys) ) {
			$this->load->model('Sessions_model', 'sessions');
			$session_id = $this->generateToken();
			/** @var CI_Session $session */
			$session = $this->session;
			$session->set_userdata('session_id', $session_id);
			$json = array(
				'session_id' => $session_id
			);
			$this->sessions->addSession($session_id);
			echo $this->getSuccResponse($json);
		}
		else {
			echo $this->getErrorResponse('API Key Invalid', 'API Key Invalid');
		}
	}

	public function home() {
		if( $this->input->server('REQUEST_METHOD') != 'POST' ) {
			$this->show_error();
			return;
		}
		if( !$this->verifyToken() ) {
			$this->invalidSessionError();
			return;
		}

		$this->load->model('News_model', 'news');
		$news = $this->news->getTopTenForRest();
		$rest = array();

		$rest['news'] = $news;

		$this->load->model('Products_model', 'products');

		$featured_products = $this->products->getFeaturedProducts();

		if( $this->isLoggedIn() ) {
			$userId = $this->getUserId();

			$totalCount = $this->products->getTotalProductCountForUser($userId);
			$pakistaniCount = $this->products->getPakistaniProductCountForUser($userId);
			if( $pakistaniCount > 0 ) {
				$pakistaniPercent = ($totalCount / $pakistaniCount) * 100;
			}
			else {
				$pakistaniPercent = 0;
			}
			$totalPercent = 100 - $pakistaniPercent;

			$rest['user_total_product_count'] = $totalCount;
			$rest['user_pakistani_product_count'] = $pakistaniCount;
			$rest['total_product_percent'] = $totalPercent;
			$rest['pakistani_product_percent'] = $pakistaniPercent;

		}
		else {
			$totalCount = $this->products->getTotalProductCount();
			$pakistaniCount = $this->products->getPakistaniProductCount();
			if( $pakistaniCount > 0 ) {
				$pakistaniPercent = ($totalCount / $pakistaniCount) * 100;
			}
			else {
				$pakistaniPercent = 0;
			}
			$totalPercent = 100 - $pakistaniPercent;

			$rest['total_product_count'] = $totalCount;
			$rest['pakistani_product_count'] = $pakistaniCount;
			$rest['total_product_percent'] = $totalPercent;
			$rest['pakistani_product_percent'] = $pakistaniPercent;
		}

		$rest['featured_products'] = $featured_products;

		echo $this->getSuccResponse($rest);

	}

	public function products()
	{
		if( $this->input->server('REQUEST_METHOD') != 'POST' ) {
			$this->show_error();
			return;
		}
		if( !$this->verifyToken() ) {
			$this->invalidSessionError();
			return;
		}
		$start = $this->input->post('start', true);
		$end = $this->input->post('end', true);
		if( !$start ) {
			$start = 0;
		}
		if( !$end ) {
			$end = 10;
		}
		$this->load->model('Products_model', 'products');
		$products = $this->products->getProductsForRest($start, $end);
		echo $this->getSuccResponse($products);
	}

	public function single_product() {
		if( $this->input->server('REQUEST_METHOD') != 'POST' ) {
			$this->show_error();
			return;
		}
		if( !$this->verifyToken() ) {
			$this->invalidSessionError();
			return;
		}
		$id = $this->input->post('id', true);
		if( !$id ) {
			$id = 0;
		}

		$this->load->model('Products_model', 'products');
		$product = $this->products->getSingleProductsForRest($id);
		echo $this->getSuccResponse($product);
	}

	public function add_product() {
		if( $this->input->server('REQUEST_METHOD') != 'POST' ) {
			$this->show_error();
			return;
		}
		if( !$this->verifyToken() ) {
			$this->invalidSessionError();
			return;
		}
		$this->load->model('User_added_products_model', 'user_added_products');

		if( $this->input->server('REQUEST_METHOD') == 'POST' ) {
			$file = $_FILES['image'];
			if( empty($file) || false == file_exists($file['tmp_name']) ) {
				echo $this->getErrorResponse('No File uploaded', 'No File uploaded');
				exit;
			}

			$filename = sha1_file($file['tmp_name']);

			$config['upload_path'] = FCPATH . PRODUCT_IMAGES_PATH;
			$config['allowed_types'] = 'gif|jpg|png';
			$config['file_name'] = $filename;
			$config['file_ext_tolower'] = true;

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('image')) {
				echo $this->getErrorResponse($this->upload->display_errors(), 'Image could not be uploaded');
				exit;
			} else {
				$upload_data = $this->upload->data();

				$newProduct = new stdClass();
				$newProduct->title = $this->input->post('title', true);
				//$newProduct->website = $this->input->post('website', true);
				$newProduct->company_id = $this->input->post('company_id', true);
				$newProduct->barcode = $this->input->post('barcode', true);
				//$newProduct->suggestions = $this->input->post('suggestions', true);
				$newProduct->category_id = $this->input->post('category_id', true);
				$newProduct->language_id = $this->input->post('language_id', true);
				$newProduct->enabled = $this->input->post('enabled', true);
				$newProduct->user_id = $this->input->post('user_id', true);
				$newProduct->image = $upload_data['file_name'];

				$addedProduct = $this->user_added_products->add_product($newProduct);
				echo $this->getSuccResponse($addedProduct);
				exit;
			}
		}
		//$this->load->view('product');
		//$product = $this->user_added_products->add($data);
		//return json_encode($product);
	}

	public function login() {
		if( $this->input->server('REQUEST_METHOD') != 'POST' ) {
			$this->show_error();
			return;
		}
		if( !$this->verifyToken() ) {
			$this->invalidSessionError();
			return;
		}
		$email = $this->input->post('email', true);
		$password = $this->input->post('password', true);

		if( !$email ) {
			$this->show_error('Email required');
			return;
		}
		if( !filter_var($email, FILTER_VALIDATE_EMAIL) ) {
			$this->show_error('Email not valid');
			return;
		}
		if( !$password ) {
			$this->show_error('Password required');
			return;
		}

		$this->ion_auth_model->tables = array(
				'users'				=> 'users',
				'groups'			=> 'groups',
				'users_groups'		=> 'users_groups',
				'login_attempts'	=> 'login_attempts',
		);

		if ($this->ion_auth_model->login($email, $password))
		{
			echo $this->getSuccResponse(array('success' => true));
		}
		else
		{
			echo $this->getSuccResponse(array('success' => false));
		}
	}

	public function register_user() {
		if( $this->input->server('REQUEST_METHOD') != 'POST' ) {
			$this->show_error();
			return;
		}
		if( !$this->verifyToken() ) {
			$this->invalidSessionError();
			return;
		}
		// passed validation
		$username = $this->input->post('email', true);
		$email = $this->input->post('email', true);
		$password = $this->input->post('password', true);

//		if( !$username ) {
//			$this->show_error('Username required');
//			return;
//		}
		if( $this->usernameAlreadyExists($username) ) {
			$this->show_error('A user with this email already exists');
			return;
		}
		if( !$email ) {
			$this->show_error('Email required');
			return;
		}
		if( !filter_var($email, FILTER_VALIDATE_EMAIL) ) {
			$this->show_error('Email not valid');
			return;
		}
		if( $this->emailAlreadyExists($email) ) {
			$this->show_error('Email already exists');
			return;
		}
		if( !$password ) {
			$this->show_error('Password required');
			return;
		}

		$identity = $email;
		$additional_data = array(
				'first_name'	=> $this->input->post('first_name', true),
				'last_name'		=> $this->input->post('last_name', true),
				'company'		=> $this->input->post('company', true),
				'phone'			=> $this->input->post('phone', true),
		);
		$groups = array(1);

		$this->ion_auth_model->tables = array(
			'users'				=> 'users',
			'groups'			=> 'groups',
			'users_groups'		=> 'users_groups',
			'login_attempts'	=> 'login_attempts',
		);

		// proceed to create user
		/** @var Ion_auth $ion_auth */
		$ion_auth = $this->ion_auth;
		$user_id = $ion_auth->register($identity, $password, $email, $additional_data, $groups);
		if ($user_id)
		{
			// directly activate user
			$this->ion_auth->activate($user_id);
			$user = $this->getUser($user_id);

			echo $this->getSuccResponse($user);
		}
		else
		{
			// failed
			$errors = $this->ion_auth->errors();
			echo $this->getErrorResponse($errors, 'User Registration failed');
		}
	}

	public function mark_as_fav() {
		if( $this->input->server('REQUEST_METHOD') != 'POST' ) {
			$this->show_error();
			return;
		}
		if( !$this->verifyToken() ) {
			$this->invalidSessionError();
			return;
		}
		$product_id = $this->input->post('product_id', true);
		$user_id = $this->input->post('user_id', true);
		if( !$product_id ) {
			$this->show_error('Product ID required');
			return;
		}
		if( !$user_id ) {
			$this->show_error('User ID required');
			return;
		}
		$this->load->model('Favorites_model', 'favorites');
		$this->favorites->mark_as_fav( $product_id, $user_id );
		echo $this->getSuccResponse(array('message' => 'favorite added'));
	}

	public function favorites() {
		if( $this->input->server('REQUEST_METHOD') != 'POST' ) {
			$this->show_error();
			return;
		}
		if( !$this->verifyToken() ) {
			$this->invalidSessionError();
			return;
		}
		$user_id = $this->input->post('user_id', true);
		if( !$user_id ) {
			$this->show_error('User ID required');
			return;
		}
		$this->load->model('Products_model', 'products');
		$products = $this->products->getFavoriteProductsForUser($user_id);
		echo $this->getSuccResponse($products);
	}

	public function contact() {
		if( $this->input->server('REQUEST_METHOD') != 'POST' ) {
			$this->show_error();
			return;
		}
		if( !$this->verifyToken() ) {
			$this->invalidSessionError();
			return;
		}
		$name = $this->input->post('name', true);
		$email = $this->input->post('email', true);
		$phone = $this->input->post('phone', true);
		$desc = $this->input->post('desc', true);
		if( !$name ) {
			$this->show_error('Name required');
			return;
		}
		if( !$email ) {
			$this->show_error('Email required');
			return;
		}
		if( !filter_var($email, FILTER_VALIDATE_EMAIL) ) {
			$this->show_error('Email not valid');
			return;
		}
		if( !$desc ) {
			$this->show_error('Desc required');
			return;
		}
		$data = array(
			'name' => $name,
			'email' => $email,
			'phone' => $phone,
			'message' => $desc,
		);
		$this->load->model('Contact_queries_model', 'queries');
		$this->queries->add_query( $data );
		echo $this->getSuccResponse(array('message' => 'Contact Query added'));
	}

	public function news()
	{
		if( $this->input->server('REQUEST_METHOD') != 'POST' ) {
			$this->show_error();
			return;
		}
		if( !$this->verifyToken() ) {
			$this->invalidSessionError();
			return;
		}
		$start = $this->input->post('start', true);
		$end = $this->input->post('end', true);
		if( !$start ) {
			$start = 0;
		}
		if( !$end ) {
			$end = 0;
		}
		$this->load->model('News_model', 'news');
		$news = $this->news->getNewsForRest($start, $end);
		echo $this->getSuccResponse($news);
	}

	public function single_news() {
		if( $this->input->server('REQUEST_METHOD') != 'POST' ) {
			$this->show_error();
			return;
		}
		if( !$this->verifyToken() ) {
			$this->invalidSessionError();
			return;
		}
		$id = $this->input->post('id', true);
		if( !$id ) {
			$id = 0;
		}
		$this->load->model('News_model', 'news');
		$news = $this->news->getSingleForRest($id);
		echo $this->getSuccResponse($news);
	}

	public function countries()
	{
		if( $this->input->server('REQUEST_METHOD') != 'POST' ) {
			$this->show_error();
			return;
		}
		if( !$this->verifyToken() ) {
			$this->invalidSessionError();
			return;
		}
		$countries = $this->config->item('country_list');
		echo $this->getSuccResponse($countries);
	}

	public function companies() {
		if( $this->input->server('REQUEST_METHOD') != 'POST' ) {
			$this->show_error();
			return;
		}
		if( !$this->verifyToken() ) {
			$this->invalidSessionError();
			return;
		}
		$start = $this->input->post('start', true);
		$end = $this->input->post('end', true);
		if( !$start ) {
			$start = 0;
		}
		if( !$end ) {
			$end = 0;
		}
		$this->load->model('Companies_model', 'companies');
		$companies = $this->companies->getCompaniesForRest($start, $end);
		echo $this->getSuccResponse($companies);
	}

	public function single_company() {
		if( $this->input->server('REQUEST_METHOD') != 'POST' ) {
			$this->show_error();
			return;
		}
		if( !$this->verifyToken() ) {
			$this->invalidSessionError();
			return;
		}
		$id = $this->input->post('id', true);
		if( !$id ) {
			$id = 0;
		}
		$this->load->model('Companies_model', 'companies');
		$company = $this->companies->getSingleForRest($id);
		echo $this->getSuccResponse($company);
	}

	public function categories() {
		if( $this->input->server('REQUEST_METHOD') != 'POST' ) {
			$this->show_error();
			return;
		}
		if( !$this->verifyToken() ) {
			$this->invalidSessionError();
			return;
		}
		$start = $this->input->post('start', true);
		$end = $this->input->post('end', true);
		if( !$start ) {
			$start = 0;
		}
		if( !$end ) {
			$end = 0;
		}
		$this->load->model('Categories_model', 'categories');
		$categories = $this->categories->getCategoriesForRest($start, $end);
		echo $this->getSuccResponse($categories);
	}

	public function sub_categories() {
		if( $this->input->server('REQUEST_METHOD') != 'POST' ) {
			$this->show_error();
			return;
		}
		if( !$this->verifyToken() ) {
			$this->invalidSessionError();
			return;
		}
		$start = $this->input->post('start', true);
		$end = $this->input->post('end', true);
		$category_id = $this->input->post('category_id', true);
		if( !$start ) {
			$start = 0;
		}
		if( !$end ) {
			$end = 0;
		}
		if( !$category_id ) {
			$category_id = null;
		}
		$this->load->model('Categories_model', 'categories');
		$categories = $this->categories->getSubCategoriesForRest($start, $end, $category_id);
		echo $this->getSuccResponse($categories);
	}

	private function generateToken()
	{
		$token = bin2hex(openssl_random_pseudo_bytes(16));
		return $token;
	}

	private function show_error($error = null) {
		if( !$error ) {
			$error = 'GET requests not allowed at this URL';
		}
//		$error = array(
//			'error' => $error
//		);
		echo $this->getErrorResponse($error, $error);
	}

	private function invalidSessionError() {
		$passed_token = $this->input->post('session_id', true);
		$session_token = $this->session->userdata('session_id');
		$error = sprintf("Session ID invalid, %s passed", $passed_token);
		echo $this->getErrorResponse($error, $error);
	}

	private function verifyToken() {
		$this->load->model('Sessions_model', 'sessions');
		$passed_token = $this->input->post('session_id', true);
		//$session_token = $this->session->userdata('session_id');
		$sessionExists = $this->sessions->getSession($passed_token);
		if( !$passed_token ) {
			return false;
		}
		elseif( !$sessionExists ) {
			return false;
		}
		return true;
	}

	private function emailAlreadyExists($email) {
		return $this->ion_auth_model->email_check($email);
	}

	private function usernameAlreadyExists($username) {
		return $this->ion_auth_model->username_check($username);
	}

	private function getUser($user_id) {
		$user = $this->ion_auth_model->user($user_id)->result();
		if( !$user ) {
			return null;
		}
		$user = $user[0];
		$return = new stdClass();
		$return->id = $user->id;
		$return->username = $user->username;
		$return->email = $user->email;
		$return->first_name = $user->first_name;
		$return->last_name = $user->last_name;
		$return->company = $user->company;
		$return->phone = $user->phone;
		return $return;
	}

	private function getSuccResponse($data){
		$respons = array();
		$respons['status'] = "true";
		$respons['statusCode'] = 200;
		$respons['statusMessage'] = "success";
		$respons['data'] = $data;
		header('Content-type: application/json');
		return json_encode($respons);
	}

	private function getErrorResponse($errors,$msg = ""){
		if( !is_array($errors) ) {
			$errors = array($errors);
		}
		$respons = array();
		$respons['status'] = "false";
		$respons['statusCode'] = 404;
		$respons['statusMessage'] = $msg;
		$respons['errors'] = $errors;
		$respons['msg'] = $msg;
		$respons['data'] = array("msg" => $msg, "errors" => $errors);
		header('Content-type: application/json');
		return json_encode($respons);
	}

	private function isLoggedIn() {
		return true;
	}

	private function getUserId() {
		return 1;
	}
}
