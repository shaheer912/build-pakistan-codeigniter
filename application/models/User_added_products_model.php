<?php 

class User_added_products_model extends MY_Model {

    private $junction_table = 'user_added_products_junction';

    public function publish($product) {
        $this->load->model('Products_model', 'products');

        $this_id = $product->id;

        $newProduct = $product;
        unset($newProduct->id);
        $newProduct->created = date('Y-m-d H:i:s');
        $newProduct->updated = date('Y-m-d H:i:s');
        $newId = $this->products->insert($newProduct);

        $sql = "SELECT product_id, alternate_product_id FROM {$this->junction_table} WHERE product_id = ?";
        $alternate_products = $this->db->query($sql, array($this_id));

        if( $alternate_products->num_rows() > 0 ) {
            $sql_for_alternate_products =
                "INSERT INTO alternate_products_junction(`product_id`, `alternate_product_id`) VALUES";
            foreach ($alternate_products->result() as $record) {
                $sql_for_alternate_products .= "('$newId', {$record->alternate_product_id}), ";
            }
            $sql_for_alternate_products = substr($sql_for_alternate_products, 0, strlen($sql_for_alternate_products) - 2);
            $this->db->query($sql_for_alternate_products);
        }

        $this->delete($this_id);
        return $newId;
    }

    public function add_product($product) {
        $product->created = date('Y-m-d H:i:s');
        $product->updated = date('Y-m-d H:i:s');
        $newId = $this->insert($product);
        return $this->get($newId);
    }
}