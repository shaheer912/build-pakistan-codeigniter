<?php 

class Categories_model extends MY_Model {

    public function getSingleForRest( $id ) {

        $category = $this->get($id);

        $category->image = $this->getImageURL($category);

        return $category;
    }

    public function getCategoriesForRest( $start = 0, $end = 10 ) {
        $categories = $this->select('id, title')->limit($end, $start)->get_many_by('parent_id IS NULL');
        return $categories;
    }

    public function getSubCategoriesForRest( $start = 0, $end = 10, $category_id = null ) {
        $this->select('id, title')->limit($end, $start);
        if( $category_id ) {
            return $this->get_many_by('parent_id', $category_id);
        }
        else {
            return $this->get_many_by('parent_id IS NOT NULL');
        }
    }

    public function getImageURL($category) {
        return base_url(CATEGORY_IMAGES_PATH) . "/{$category->image}";
    }
}