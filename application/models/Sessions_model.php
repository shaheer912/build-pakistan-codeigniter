<?php 

class Sessions_model extends MY_Model {

    public function addSession( $session_id ) {

        $data = array(
            'session_id' => $session_id,
            'created' => date('Y-m-d H:i:s')
        );

        $insert_id = $this->insert($data);

        return $insert_id;
    }

    public function getSession( $session_id ) {
        $session = $this->get_by('session_id', $session_id);
        return $session;
    }

}