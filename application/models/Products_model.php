<?php 

class Products_model extends MY_Model {

    private $junction_table = 'alternate_products_junction';
    private $table_companies = 'companies';
    private $countries = array();

    public function __construct()
    {
        $this->countries = $this->config->item('country_list');
        parent::__construct();
    }

    public function getSingleProductsForRest( $id ) {

        $sql = "
        SELECT p.id as id, p.title as title, p.image, c.title as company_name, c.country_code, b.title as brand, cc.title as sub_cat, ccp.title as cat
        FROM `products` p
        LEFT JOIN companies b ON b.id = p.`company_id`
        LEFT JOIN companies c ON c.id = b.`parent_id`
        LEFT JOIN categories cc ON cc.id = p.category_id
        LEFT JOIN categories ccp ON ccp.id = cc.parent_id
        WHERE p.id = ?
        ";
        $product = $this->db->query($sql, array($id))->row();
//
//        $product = $this->get($id);
//
        $alternates = $this->get_alternate_products($product);
        $product->alternates = $alternates;
        $product->image = $this->getProductImageURL($product);
        $product->country_name = $this->countries[$product->country_code];
        unset($product->country_code);

        return $product;
    }

    public function getProductsForRest( $start = 0, $end = 10, $include_alternates = false ) {

        $sql = "
        SELECT p.id as product_id, p.title as product_title, p.image, c.title as company_name, c.country_code, b.title as brand
        FROM `products` p
        LEFT JOIN companies b ON b.id = p.`company_id`
        LEFT JOIN companies c ON c.id = b.`parent_id`
        LIMIT {$start}, {$end}
        ";
        $productList = array();
        /** @var CI_DB_mysqli_result $products */
        $products = $this->db->query($sql);
        foreach( $products->result() as $product ) {

            $productObj = new stdClass();
            $productObj->id = $product->product_id;
            $productObj->title = $product->product_title;
            $productObj->company_name = $product->company_name;
            $productObj->brand = $product->brand;
            $productObj->image_url = $this->getProductImageURL($product);
            if( $include_alternates ) {
                $alternates = $this->get_alternate_products($product);
                $product->alternates = $alternates;
            }
            $productObj->country_name = $this->countries[$product->country_code];
            $productList[] = $productObj;
        }

        $count = $this->count_all();
        $return['products'] = $productList;
        $return['count'] = $count;
        $return['current_start'] = $start;
        $return['current_end'] = $end;
        return $return;
    }

    public function getFavoriteProductsForUser($user_id) {
        $productIdList = $this->_getFavoriteProductIDsForUser($user_id);
        $results = $this->get_many_by(
            array(
                'id' => $productIdList
            )
        );
        $products = array();
        foreach( $results as $result ) {
            $products[] = array(
                'product_id' => $result->id,
                'product_title' => $result->title,
                'short_desc' => $result->suggestions,
            );
        }
        return $products;
    }

    public function getFeaturedProducts() {
        $results = $this->limit(10)->get_many_by('featured', 1);;
        $products = array();
        foreach( $results as $result ) {
            $products[] = array(
              'product_id' => $result->id,
              'product_title' => $result->title,
              'short_desc' => $result->suggestions,
            );
        }

        return $products;
    }

    public function getTotalProductCount() {
        $total = $this->count_all();
        return $total;
    }

    public function getPakistaniProductCount() {
        $sql = "SELECT COUNT(id) as total FROM {$this->_table} WHERE company_id IN (select id FROM {$this->table_companies} WHERE country_code='PK')";
        $result = $this->db->query($sql);
        $result = $result->result();
        $total = $result[0]->total;
        return $total;
    }

    public function getTotalProductCountForUser($user_id) {
        $this->load->model('Favorites_model', 'favorites');
        $total = $this->favorites->count_by( 'user_id', $user_id );
        return $total;
    }

    public function getPakistaniProductCountForUser($user_id) {
        $productIdList = $this->_getFavoriteProductIDsForUser($user_id);
        $sql = "SELECT COUNT(p.id) as total
                FROM {$this->_table} p
                WHERE p.company_id IN (select id FROM {$this->table_companies} WHERE country_code='PK')
                AND p.id IN ?
                ";
        $result = $this->db->query($sql, array($productIdList));
        $result = $result->result();
        $total = $result[0]->total;
        return $total;
    }

    public function get_alternate_products($product) {
        $sql = "SELECT alternate_product_id as id, p.title as title, p.image, c.title as company_name
        FROM {$this->junction_table}
        INNER JOIN products p ON p.id = alternate_products_junction.id
        INNER JOIN companies b ON b.id = p.`company_id`
        INNER JOIN companies c ON c.id = b.`parent_id`
        WHERE product_id = ?";

        $alternate_products = $this->db->query($sql, array($product->id));
        $return = array();
        foreach( $alternate_products->result() as $alternate_product ) {
            $alternate_product->image_url = $this->getProductImageURL($alternate_product);
            unset($alternate_product->image);
            $return[] = $alternate_product;
        }
        return $return;
    }

    public function getProductImageURL($product) {
        return base_url(PRODUCT_IMAGES_PATH) . "/{$product->image}";
    }

    private function _getFavoriteProductIDsForUser($user_id) {
        $this->load->model('Favorites_model', 'favorites');
        $productIdResults = $this->favorites->select('product_id')->get_many_by( 'user_id', $user_id );
        $productIdList = array();
        foreach( $productIdResults as $productIdResult ) {
            $productIdList[] = $productIdResult->product_id;
        }
        return $productIdList;
    }
}