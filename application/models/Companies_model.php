<?php 

class Companies_model extends MY_Model {

    public function getSingleForRest( $id ) {

        $news = $this->get($id);

        $news->image = $this->getImageURL($news);

        return $news;
    }

    public function getCompaniesForRest( $start = 0, $end = 10 ) {

        $companies = $this->select('id, title')->limit($end, $start)->get_many_by('parent_id IS NULL');
        return $companies;
    }

    public function getBrandsForRest( $start = 0, $end = 10 ) {

        $companies = $this->select('id, title')->limit($end, $start)->get_many_by('parent_id IS NOT NULL');
        return $companies;
    }

    public function getImageURL($company) {
        return base_url(COMPANY_IMAGES_PATH) . "/{$company->image}";
    }
}