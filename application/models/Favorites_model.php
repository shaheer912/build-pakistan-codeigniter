<?php 

class Favorites_model extends MY_Model {
    protected $_table = 'product_favorites';

    public function getSingleForRest( $id ) {

        $news = $this->get($id);

        $news->image = $this->getImageURL($news);

        return $news;
    }

    public function mark_as_fav( $product_id, $user_id ) {

        $data = array(
            'product_id' => $product_id,
            'user_id' => $user_id
        );
        $exists = $this->get_by($data);
        if( !$exists ) {
            $id = $this->insert(
                $data
            );
            return $id;
        }
        return $exists->id;
    }

    public function getBrandsForRest( $start = 0, $end = 10 ) {

        $companies = $this->select('id, title')->limit($end, $start)->get_many_by('parent_id IS NOT NULL');
        return $companies;
    }

    public function getImageURL($company) {
        return base_url(COMPANY_IMAGES_PATH) . "/{$company->image}";
    }
}