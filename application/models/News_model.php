<?php 

class News_model extends MY_Model {

    public function getSingleForRest( $id ) {

        $newsItem = $this->get($id);

        $newsItem->image = $this->getImageURL($newsItem);
        $newsObj = new stdClass();
        $newsObj->id = $newsItem->id;
        $newsObj->title = $newsItem->title;
        $newsObj->image = $this->getImageURL($newsItem);
        $newsObj->video = $newsItem->video;
        $newsObj->html_content = $newsItem->content;
        $newsObj->date = $newsItem->created;

        return $newsObj;
    }

    public function getNewsForRest( $start = 0, $end = 10 ) {

        $news = $this->limit($end, $start)->get_all();

        $newsList = array();
        foreach( $news as $newsItem ) {
            $newsObj = new stdClass();
            $newsObj->news_id = $newsItem->id;
            $newsObj->news_title = $newsItem->title;
            $newsObj->short_desc = strip_tags($newsItem->content);
            $newsObj->news_image = $this->getImageURL($newsItem);
            $newsList[] = $newsObj;
        }
        $count = $this->count_all();
        $return['news'] = $newsList;
        $return['count'] = $count;
        $return['current_start'] = $start;
        $return['current_end'] = $end;
        return $return;
    }

    public function getTopTenForRest()
    {
        $news = $this->limit(10, 0)->order_by('id', 'desc')->get_all();

        $newsList = array();
        foreach( $news as $newsItem ) {
            $newsObj = new stdClass();
            $newsObj->news_id = $newsItem->id;
            $newsObj->news_title = $newsItem->title;
            $newsObj->short_desc = strip_tags($newsItem->content);
            $newsObj->news_image = $this->getImageURL($newsItem);
            $newsList[] = $newsObj;
        }
        $count = $this->count_all();
        $return['news'] = $newsList;
        $return['count'] = $count;
        return $return;
    }

    public function getImageURL($news) {
        return base_url(NEWS_IMAGES_PATH) . "/{$news->image}";
    }
}